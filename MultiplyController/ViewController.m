//
//  ViewController.m
//  MultiplyController
//
//  Created by Student on 26.11.2019.
//  Copyright © 2019 Student. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // init
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier  isEqual: @"firstSegue"]) {
        SecondViewController *sc = segue.destinationViewController;
        sc.text  = @"Text text";
    }
    
}


@end
