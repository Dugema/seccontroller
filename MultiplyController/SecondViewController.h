//
//  SecondViewController.h
//  MultiplyController
//
//  Created by Student on 26.11.2019.
//  Copyright © 2019 Student. All rights reserved.
//

#import "ViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SecondViewController : ViewController
@property NSString *text;
@end

NS_ASSUME_NONNULL_END
